import os, sys, time
from multiprocessing import Process

class Speech:
    def speak(self, sent):
        os.system('speak "' + sent + '"')

    def sayJoke(self, jokeList, emotionList, emo):
        i = 0
        while i < len(jokeList):
            p1 = None
            if i <= len(emotionList) and i != 0:
                a = emotionList[i - 1]
                p1 = Process(target=emo.startEmotion, args=(a,))
            b = jokeList[i]
            p2 = Process(target=self.speak, args=(b,))
            if p1 is not None:
                p1.start()
            p2.start()
            if p1 is not None:
                p1.join()
            p2.join()
            i = i + 1