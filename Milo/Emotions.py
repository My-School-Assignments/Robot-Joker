import os, sys, time

class Emotions:
    def startEmotion(self, emot):
        if emot == '0':
            self.neutral()
        elif emot == '1':
            self.happy()
        elif emot == '2':
            self.sad()
        elif emot == '3':
            self.angry()
        elif emot == '4':
            self.disgusted()
        elif emot == '5':
            self.afraid()
        elif emot == '6':
            self.surprise()

    def neutral(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_defaults.anim.xml')
        time.sleep(1)
    def sad(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_sad_01.anim.xml')
        time.sleep(1)
    def happy(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_happy_01.anim.xml')
        time.sleep(1)
    def angry(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_angry_01.anim.xml')
        time.sleep(1)
    def disgusted(self):
        os.system('anim-play  /usr/robokind/etc/gui/anims/AZR25_peeved_01.anim.xml')
        time.sleep(1)
    def afraid(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_scared_02_4000.anim.xml')
        time.sleep(1)
    def neutral(self):
        return
    def denial(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_denial_01.anim.xml')
        time.sleep(1)
    def surprise(self):
        os.system('anim-play /usr/robokind/etc/gui/anims/AZR25_surprise_01.anim.xml')
        time.sleep(1)