from naoqi import ALProxy
import motion
import almath

class Emotions:
    def __init__(self, ip, postureProxy):
        self.postureProxy = postureProxy
        self.ip = ip
        self.leds = ALProxy("ALLeds", ip, 9559)

    def neutral(self):
        self.leds.post.fadeRGB("FaceLeds",0xFFFFFF,0.1)
    def happiness(self):
        self.leds.post.fadeRGB("FaceLeds", 0x00FF00 , 0.1)
    def sadness(self):
        self.leds.post.fadeRGB("FaceLeds",0x4B0082, 0.1)
    def anger(self):
        self.leds.post.fadeRGB("FaceLeds",0xFF0000, 0.1)
    def disgust(self):
        self.leds.post.fadeRGB("FaceLeds",0xEEE8AA , 0.1)
    def fear(self):
        self.leds.post.fadeRGB("FaceLeds",0x000000, 0.1)
