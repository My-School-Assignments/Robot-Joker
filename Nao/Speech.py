# -*- coding: utf-8 -*-
import time
import os, sys
import Emotions
sys.path.insert(1, os.path.join(sys.path[0], '../Common/'))
from Parser import *
from Communication import *
from Camera import *
from naoqi import ALProxy
import json as JSON

class Speech:
    def __init__(self,ip,port,asp,postureProx):
        self.robotIP=ip
        self.port = port
        self.postureProxy=postureProx
        self.animatedSpeechProxy = asp
        self.happinessPred = 0.5
        self.happinessPo = 0.5
        self.id = None

        self.camera = Camera(self.robotIP)
        self.com = Communication()
        self.emo = Emotions.Emotions(self.robotIP,self.postureProxy)
        #self.postureProxy.goToPosture("StandInit", 0.7)
        self.postureProxy.goToPosture("Sit", 0.7)




    def speak(self,jL,eL,id):
        self.jokeList=jL
        self.emotionList=eL
        self.id=id

        self.camera.takePhoto()
        photoString = open("pic.png",'rb').read()
        #json = self.com.emoRequest(photoString)
        jsonikPred = JSON.loads(self.com.emoRequest(photoString).decode())
        if jsonikPred != []:
            self.happinessPred = jsonikPred[0]['scores']['happiness']
        i=0
        while i<len(self.jokeList):
            if i<=len(self.emotionList) and i!=0:
                if self.emotionList[i-1]=='0':
                    self.emo.neutral()
                elif self.emotionList[i-1]=='1':
                    self.emo.happiness()
                elif self.emotionList[i-1]=='2':
                    self.emo.sadness()
                elif self.emotionList[i - 1] == '3':
                    self.emo.anger()
                elif self.emotionList[i - 1] == '4':
                    self.emo.sadness()
                elif self.emotionList[i - 1] == '5':
                    self.emo.disgust()
                elif self.emotionList[i - 1] == '6':
                    self.emo.fear()
            #tts.setVolume(1.0) #Nao 1.14
            #tts.say(jokeList[i]) #Nao 1.14
            configuration = {"bodyLanguageMode": "contextual"}
            #self.animatedSpeechProxy.say("Prišla si z vyjebanej dediny, väčšinou pochádzaš z chudobnej rodiny, za cieľom splniť si svoje vidiny, si vyjebaná zlatokopka.. Ide ti len iba o love, od začiatku ideš len na hotové, darčeky, kabelky Loui Vuittonové, si vyjebaná zlatokopka.. Čávo môže byť aj rapavý, ale na tom ti vôbec nezáleží, dôležité je, že je lovatý, si pojebaná zlatokopka..")
            self.animatedSpeechProxy.say(str(self.jokeList[i]), configuration) #Nao 2.14
            i=i+1
        photoString = self.camera.takePhoto()
        photoString = open("pic.png",'rb').read()
        jsonikPo = JSON.loads(self.com.emoRequest(photoString).decode())
        if jsonikPo != []:
            self.happinessPo = jsonikPo[0]['scores']['happiness']
        self.com.sendEmo(self.id,self.happinessPred,self.happinessPo,"nao")


