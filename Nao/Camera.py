# -*- encoding: UTF-8 -*-
# Get an image from NAO. Display it and save it using PIL.

# Python Image Library
from PIL import Image

from naoqi import ALProxy

class Camera:
    def __init__(self,ip):
        self.IP=ip
        self.PORT=9559
    def takePhoto(self):
        cameraID=0
        camProxy = ALProxy("ALVideoDevice", self.IP, self.PORT)
        camProxy.setActiveCamera(cameraID)
        temp = camProxy.getActiveCamera()

        resolution = 2    # VGA
        colorSpace = 11   # RGB

        videoClient = camProxy.subscribe("python_client", resolution, colorSpace, 5)

        # Get a camera image.
        # image[6] contains the image data passed as an array of ASCII chars.
        naoImage = camProxy.getImageRemote(videoClient)

        camProxy.unsubscribe(videoClient)

          # Now we work with the image returned and save it as a PNG  using ImageDraw
          # package.

          # Get the image size and pixel array.
        imageWidth = naoImage[0]
        imageHeight = naoImage[1]
        array = naoImage[6]


        # Create a PIL Image from our pixel array.
        im = Image.frombytes("RGB", (imageWidth, imageHeight), array)

          # Save the image.
        im.save("pic.png", "PNG")


        #return im