# Robot Joker
The purpose of this project is to program robots Nao and Milo to tell jokes. After they tell a joke they record a face of the user and using Azure Emotion API detect his emotion. Robots store those emotion data in cloud database. The data can be investigated using special client app for Windows. The project was developed as a Humanoid Technologies assignment.
---
### Video Presentation
https://youtu.be/un8mpwhtldk
### Python libraries required:
```python 
-m pip install pymongo
PyNaoQi
PIL
```
### Basic Emotions
---
0. neutral (poker face)
1. happiness
2. sadness
3. anger
4. disgust
5. fear
6. contempt opovrhnutie
7. surprise
---
© Dominik Horňák, Richard Halčin, Dominik Grigľak, Simona Korkobcová