﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace RobotJoker
{
    class Joke
    {
        private string category;
        private string text;
        private BsonDocument joke;

        public Joke(string text, string category)
        {
            this.category = category;
            this.text = text;
            
            this.joke = new BsonDocument
            {
                {"category",this.category},
                {"text",this.text}
            };
        }
        
        public BsonDocument getJoke()
        {
            return this.joke;
        }

        public void setJoke(BsonDocument newJoke)
        {
            this.joke = newJoke;
        }

    }
}
