﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Input;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Text.RegularExpressions;
using Renci.SshNet;
using Emgu.CV;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Controls;
using Emgu.CV.Structure;
using Newtonsoft.Json.Linq;
using Point = System.Windows.Point;

using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace RobotJoker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        private const string connectionString = @"mongodb://joker-db:wsVD6g4NCiun4viFm0xVAzbWZjTyITeQXeapXiLBP5T12DklVzuQl7EqzPigOOyJaHIGtEzFDjB5oEj5OdVdxQ==@joker-db.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
        private MongoClient client;
        private IMongoDatabase database;

        private SshClient sshClient;
        private bool filterPG13Jokes = false;
        public List<Point> pointsToDrawBefore;
        public List<Point> pointsToDrawAfter;

        public MainWindow()
        {
            InitializeComponent();
            pointsToDrawBefore = new List<Point>();
            pointsToDrawAfter = new List<Point>();
        }

        // functions for adding emotion into joke text string 
        #region Clickcking Emotions
        private void ClickHappy(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/1 ");
        }
        private void ClickSad(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/2 ");
        }
        private void ClickAngry(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/3 ");
        }
        private void ClickDisgust(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/4 ");
        }
        private void ClickFear(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/5 ");
        }
        private void ClickContempt(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/6 ");
        }
        private void ClickSurprise(object sender, RoutedEventArgs e)
        {
            jokeString.Text = jokeString.Text.Insert(jokeString.CaretIndex, "/7 ");
        }
        #endregion

        // functions for selecting category of joke
        #region Categories
        private void ClickSexual(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Sexual";
        }

        private void ClickDrugs(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Drugs";
        }

        private void ClickIntelligence(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Intelligence";
        }

        private void ClickBaby(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Baby";
        }

        private void ClickAlcohol(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Alcohol";
        }

        private void ClickBlack(object sender, RoutedEventArgs e)
        {
            categoryInput.Text = "Black";
        }
        #endregion

        // puts all jokes from database into ListBox for joke editting
        private void RetrieveData(object sender, RoutedEventArgs e)
        {
            editJokeListBox.ItemsSource = getJokesFromDatabase();
        }

        // saves new joke with respect on 2 conditions
        //    1. joke with given string doesn't exist already
        //    2. joke isn't empty string
        private void SaveJoke(object sender, RoutedEventArgs e)
        {
            if (jokeString.Text != null && categoryInput.Text != null)
            {
                List<string> allJokesText = getJokesFromDatabase();
                bool isUnique = true;

                foreach (string s in allJokesText)
                {
                    if (s.Equals(jokeString.Text))
                    {
                        isUnique = false;
                    }
                }

                if (isUnique)
                {
                    client = new MongoClient(connectionString);
                    database = client.GetDatabase("joker-db");

                    Joke joke = new Joke(jokeString.Text, categoryInput.Text);
                    database.GetCollection<BsonDocument>("jokes").InsertOne(joke.getJoke());

                    database = null;
                    client = null;
                }
                else
                {
                    uniqueJoke.Text = "Joke already exists!";
                }
            }
        }

        // used for robot control - sending scripts to Nao / Milo for reading given joke, connecting to robots etc.
        #region Robot_Control
        private void NumberValidationTextBox(Object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void MiloTellJokes(Object sender, EventArgs e)
        {
            string ip = MiloIp.Text;
            string username = MiloUsername.Text;
            string pwd = MiloPwd.Password;

            if (!IsValidIP(ip) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(pwd))
            {
                MessageBox.Show("The IP adress or other fields are not valid!");
                return;
            }

            try
            {
                this.sshClient = new SshClient(ip, username, pwd);
                this.sshClient.Connect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    "There was a connection problem! Make sure the robot is on and connected to the network and that your credentials are correct!\n" + ex.Message);
                return;
            }

            if (!this.sshClient.IsConnected)
            {
                MessageBox.Show(
                    "There was a connection problem! Make sure the robot is on and connected to the network and that your credentials are correct!");
                return;
            }

            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            var collection = database.GetCollection<BsonDocument>("jokes");
            List<BsonDocument> jokes;

            if (!filterPG13Jokes)
            {
                jokes = collection.Find(j => true).ToList();
            }
            else
            {
                jokes = collection.Find(j => j["category"] != "sexual" /* TODO: Add other inapropriate categories*/).ToList();
            }

            jokes.Shuffle();
            collection = database.GetCollection<BsonDocument>("tells");
            var num = 0;

            foreach (var joke in jokes)
            {
                num++;
                if (num > (string.IsNullOrEmpty(NumberOfJokes.Text) ? 1 : Int32.Parse(NumberOfJokes.Text))) break;

                var capture = new VideoCapture();
                var imageBefore = capture.QueryFrame();
                imageBefore.ToImage<Bgr, Byte>().Save(@"C:\tmp\testImage.jpg");
                var imB = System.IO.File.ReadAllBytes(@"C:\tmp\testImage.jpg");

                var taskBefore = Task<HttpResponseMessage>.Factory.StartNew(() => getImageEmotionsAsync(imB).Result);

                SendCommand("python ~/RobotJoker/Milo/Main.py \"" + joke["text"] + "\"");
                Thread.Sleep(500);

                var imageAfter = capture.QueryFrame();
                imageAfter.ToImage<Bgr, Byte>().Save(@"C:\tmp\testImage2.jpg");
                var imA = System.IO.File.ReadAllBytes(@"C:\tmp\testImage2.jpg");
                var taskAfter = Task<HttpResponseMessage>.Factory.StartNew(() => getImageEmotionsAsync(imA).Result);

                capture.Dispose();

                Task.WaitAll(taskBefore, taskAfter);

                if (taskBefore.Result.StatusCode != HttpStatusCode.OK) // Error processing faces
                {
                    continue;
                }

                if (taskAfter.Result.StatusCode != HttpStatusCode.OK) // Error processing faces
                {
                    continue;
                }

                double happinessBefore = 0;
                double numFaces = 0;
                string res = taskBefore.Result.Content.ReadAsStringAsync().Result;
                if (res == "[]")
                {
                    continue;
                }

                var root = JArray.Parse(res).First;
                var sc = root.Last.Children();

                foreach (var s in sc)
                {
                    string str = s.First.Next.Next.Next.Next.ToString();
                    str = str.Substring(12);
                    happinessBefore += Double.Parse(str);
                    numFaces++;
                }

                happinessBefore /= numFaces;

                double happinessAfter = 0;
                numFaces = 0;
                res = taskAfter.Result.Content.ReadAsStringAsync().Result;
                if (res == "[]")
                {
                    continue;
                }

                root = JArray.Parse(res).First;
                sc = root.Last.Children();

                foreach (var s in sc)
                {
                    string str = s.First.Next.Next.Next.Next.ToString();
                    str = str.Substring(12);
                    happinessAfter += Double.Parse(str);
                    numFaces++;
                }

                happinessAfter /= numFaces;

                collection.InsertOne(new BsonDocument()
                {
                    {"joke", joke["_id"]},
                    {"happinessBefore", happinessBefore},
                    {"happinessAfter", happinessAfter},
                    {"date", DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds},
                    {"robot", "milo"}
                });
            }

            database = null;
            client = null;

            try
            {
                this.sshClient.Disconnect();
                this.sshClient.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection Error! " + ex.Message);
            }
        }

        private async Task<HttpResponseMessage> getImageEmotionsAsync(Byte[] image)
        {
            var httpClient = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "a803ed32771f4d7487b60cbaa8d4f9df");
            var uri = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize?" + queryString;
            using (var content = new ByteArrayContent(image))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                return await httpClient.PostAsync(uri, content);
            }
        }

        private void NaoTellJokes(Object sender, EventArgs e)
        {
            string ip = NaoIp.Text;

            if (!IsValidIP(ip))
            {
                MessageBox.Show("The IP adress is not valid!");
                return;
            }

            string fileName = @"Nao\main.py " + ip + " " + (string.IsNullOrEmpty(NumberOfJokes.Text) ? "1" : NumberOfJokes.Text) + " " + (filterPG13Jokes ? 1 : 0);

            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(@"C:\Python27\python.exe", fileName)
            {
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };
            p.Start();

            string output = p.StandardOutput.ReadToEnd(); // ....
            p.WaitForExit();
        }

        private void FilterChanged(Object sender, RoutedEventArgs e)
        {
            if ((RadioButton)(sender) == this.FilterPG13)
            {
                if (NoFilterPG13 != null)
                {
                    NoFilterPG13.IsChecked = false;
                }
                filterPG13Jokes = true;
            }
            else if ((RadioButton)(sender) == this.NoFilterPG13)
            {
                if (FilterPG13 != null)
                {
                    this.FilterPG13.IsChecked = false;
                }
                filterPG13Jokes = false;
            }
        }

        /// <summary>
        /// Sends command to robot through SSH connection
        /// </summary>
        /// <param name="Command">The command.</param>
        private void SendCommand(string command)
        {
            if (sshClient == null)
            {
                return;
            }

            try
            {
                var stream = this.sshClient.CreateCommand(command + "; echo end-of-joke");
                var cmd = stream.BeginExecute();
                var res = stream.EndExecute(cmd);
                if (res.Contains("end-of-joke"))
                {
                    Thread.Sleep(8000);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Runn Command Error: " + ex.Message);
            }
        }

        private bool IsValidIP(string addr)
        {
            //create our match pattern
            string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //create our Regular Expression object
            Regex check = new Regex(pattern);
            //boolean variable to hold the status
            bool valid = false;
            //check to make sure an ip address was provided
            if (addr == "")
            {
                //no address provided so return false
                valid = false;
            }
            else
            {
                //address provided so use the IsMatch Method
                //of the Regular Expression object
                valid = check.IsMatch(addr, 0);
            }
            //return the results
            return valid;
        }
        #endregion // Robot_Control

        // action triggered by selecting joke from ListBox for joke editting
        // returns joke text string and category into textboxes
        private void editJokeListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            jokeEdit.Text = editJokeListBox.SelectedItem.ToString();

            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            List<BsonDocument> list = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();

            string oneJoke = list.ElementAt(editJokeListBox.SelectedIndex).ToString();
            categoryEdit.Text = parseJokeCategory(oneJoke);

            client = null;
            database = null;
        }

        // returns list of all joke's text strings as List<string>
        private List<string> getJokesFromDatabase()
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");
            List<BsonDocument> listJokes = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();
            List<string> listJokesStrings = new List<string>();

            for (int i = 0; i < listJokes.Count(); i++)
            {
                listJokesStrings.Add(listJokes.ElementAt(i).ToString());
            }

            for (int i = 0; i < listJokes.Count(); i++)
            {
                listJokesStrings[i] = parseJokeText(listJokesStrings[i]);
            }

            client = null;
            database = null;
            return listJokesStrings;
        }

        // parsing of joke text, joke id, joke category, tell joke from given string
        #region Parsers
        private string parseJokeText(string jokeText)
        {
            string text = null;
            for (int i = 0; i < jokeText.Length - 9; i++)
            {
                if (
                    jokeText.ElementAt(i).Equals('"') &&
                    jokeText.ElementAt(i + 1).Equals('t') &&
                    jokeText.ElementAt(i + 2).Equals('e') &&
                    jokeText.ElementAt(i + 3).Equals('x') &&
                    jokeText.ElementAt(i + 4).Equals('t') &&
                    jokeText.ElementAt(i + 5).Equals('"') &&
                    jokeText.ElementAt(i + 6).Equals(' ') &&
                    jokeText.ElementAt(i + 7).Equals(':') &&
                    jokeText.ElementAt(i + 8).Equals(' ') &&
                    jokeText.ElementAt(i + 9).Equals('"')
                   )
                {
                    for (int j = i + 10; j < jokeText.Length; j++)
                    {
                        if (jokeText.ElementAt(j) != '"')
                        {
                            text += jokeText.ElementAt(j);
                        }
                        else if (jokeText.ElementAt(j) == '"')
                        {
                            j = jokeText.Length;
                        }
                    }
                }
            }
            return text;
        }

        private string parseJokeId(string jokeText)
        {
            string text = null;
            for (int i = 0; i < jokeText.Length - 9; i++)
            {
                if (
                    jokeText.ElementAt(i).Equals('"') &&
                    jokeText.ElementAt(i + 1).Equals('_') &&
                    jokeText.ElementAt(i + 2).Equals('i') &&
                    jokeText.ElementAt(i + 3).Equals('d') &&
                    jokeText.ElementAt(i + 5).Equals('"') &&
                    jokeText.ElementAt(i + 6).Equals(' ') &&
                    jokeText.ElementAt(i + 7).Equals(':') &&
                    jokeText.ElementAt(i + 8).Equals(' ')
                   )
                {
                    for (int j = i + 10; j < jokeText.Length; j++)
                    {
                        if (jokeText.ElementAt(j) != '"' || jokeText.ElementAt(j + 1) != ')')
                        {
                            text += jokeText.ElementAt(j);
                        }
                        else if (jokeText.ElementAt(j) == '"')
                        {
                            j = jokeText.Length;
                        }
                    }
                }
            }
            return text;
        }

        private string parseJokeCategory(string jokeText)
        {
            string text = null;
            for (int i = 0; i < jokeText.Length - 13; i++)
            {
                if (
                    jokeText.ElementAt(i).Equals('"') &&
                    jokeText.ElementAt(i + 1).Equals('c') &&
                    jokeText.ElementAt(i + 2).Equals('a') &&
                    jokeText.ElementAt(i + 3).Equals('t') &&
                    jokeText.ElementAt(i + 4).Equals('e') &&
                    jokeText.ElementAt(i + 5).Equals('g') &&
                    jokeText.ElementAt(i + 6).Equals('o') &&
                    jokeText.ElementAt(i + 7).Equals('r') &&
                    jokeText.ElementAt(i + 8).Equals('y') &&
                    jokeText.ElementAt(i + 9).Equals('"') &&
                    jokeText.ElementAt(i + 10).Equals(' ') &&
                    jokeText.ElementAt(i + 11).Equals(':') &&
                    jokeText.ElementAt(i + 12).Equals(' ') &&
                    jokeText.ElementAt(i + 13).Equals('"')
                   )
                {
                    for (int j = i + 14; j < jokeText.Length; j++)
                    {
                        if (jokeText.ElementAt(j) != '"')
                        {
                            text += jokeText.ElementAt(j);
                        }
                        else if (jokeText.ElementAt(j) == '"')
                        {
                            j = jokeText.Length;
                        }
                    }
                }
            }
            return text;
        }

        private string parseTellsIdOfJoke(string tellText)
        {
            string text = null;
            for (int i = 0; i < tellText.Length - 8; i++)
            {
                if (
                    tellText.ElementAt(i).Equals('"') &&
                    tellText.ElementAt(i + 1).Equals('j') &&
                    tellText.ElementAt(i + 2).Equals('o') &&
                    tellText.ElementAt(i + 3).Equals('k') &&
                    tellText.ElementAt(i + 4).Equals('e') &&
                    tellText.ElementAt(i + 5).Equals('"') &&
                    tellText.ElementAt(i + 6).Equals(' ') &&
                    tellText.ElementAt(i + 7).Equals(':') &&
                    tellText.ElementAt(i + 8).Equals(' ')
                   )
                {
                    for (int j = i + 9; j < tellText.Length; j++)
                    {
                        if (tellText.ElementAt(j) != '"' || tellText.ElementAt(j + 1) != ')')
                        {
                            text += tellText.ElementAt(j);
                        }
                        else if (tellText.ElementAt(j) == '"' && tellText.ElementAt(j + 1) == ')')
                        {
                            j = tellText.Length;
                        }
                    }
                }
            }
            if (text != null)
            {
                text += '"';
                text += ')';
            }

            return text;
        }
        #endregion

        // action triggered by pressing Save editted joke
        // updates selected joke after changing its text string and/or category in textboxes
        private void EditJoke(object sender, RoutedEventArgs e)
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            if (jokeEdit.Text != null && categoryEdit != null && editJokeListBox.SelectedItem != null)
            {
                Console.WriteLine("Text in editor: " + jokeEdit.Text);
                Console.WriteLine("Text in selecting: " + editJokeListBox.SelectedItem.ToString());

                var filter = Builders<BsonDocument>.Filter.Eq("text", editJokeListBox.SelectedItem.ToString());
                var update = Builders<BsonDocument>.Update.Set("text", jokeEdit.Text);
                var updateCat = Builders<BsonDocument>.Update.Set("category", categoryEdit.Text);
                //database.GetCollection<BsonDocument>("jokes").InsertOne(joke.getJoke());
                database.GetCollection<BsonDocument>("jokes").UpdateOne(filter, update);
                database.GetCollection<BsonDocument>("jokes").UpdateOne(filter, updateCat);
            }



            //            db.students.updateOne(
            //   { _id: 1, grades: 80 },
            //   { $set: { "grades.$" : 82 } }
            //)

            //List<BsonDocument> list = database.GetCollection<BsonDocument>("jokes").Find(x => x["text"] == categoryEdit.Text).ToList();

            client = null;
            database = null;
        }

        // action triggered by pressing Load jokes
        // loads all jokes with at least one tell
        private void AllJokesIntoListBoxPlot(object sender, RoutedEventArgs e)
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            List<BsonDocument> tells = database.GetCollection<BsonDocument>("tells").Find(x => x["happinessBefore"] != 0.5 && x["happinessAfter"] != 0.5).ToList();
            List<BsonDocument> jokes = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();

            List<BsonDocument> jokesWithTell = new List<BsonDocument>();

            for (int i = 0; i < jokes.Count; i++)
            {
                for (int j = 0; j < tells.Count; j++)
                {
                    if (tells.ElementAt(j)["joke"] == jokes.ElementAt(i)["_id"])
                    {
                        jokesWithTell.Add(jokes.ElementAt(i));
                        break;
                    }
                }
            }

            double maxHappiness = -1;
            string funniestText = " ";
            foreach (var i in jokesWithTell)
            {
                Console.WriteLine(i.ToString());
                double happy = computeHappinessOfJoke(i);
                if (happy > maxHappiness)
                {
                    maxHappiness = happy;
                    funniestText = i["text"].ToString();
                }
            }

            List<string> jokesOnlyTexts = new List<string>();
            foreach (var j in jokesWithTell)
            {
                jokesOnlyTexts.Add(j["text"].ToString());
            }
            plotListBox.ItemsSource = jokesOnlyTexts;
            string text2funny = " (value : " + Math.Round(maxHappiness, 4).ToString() + " )";
            funniestText += text2funny;
            funniest.Text = funniestText;

            client = null;
            database = null;
        }

        // plots selected joke's tell's happiness values
        private void PlotJoke(object sender, RoutedEventArgs e)
        {
            if (plotListBox.SelectedItem != null)
            {
                client = new MongoClient(connectionString);
                database = client.GetDatabase("joker-db");
                string selected = plotListBox.SelectedItem.ToString();
                List<BsonDocument> jokes = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();

                var idOfSelected = jokes.Find(x => x["text"] == selected)["_id"];

                List<BsonDocument> tells = database.GetCollection<BsonDocument>("tells").Find(x => x["joke"] == idOfSelected && x["happinessBefore"] != 0.5 && x["happinessAfter"] != 0.5).ToList();
                List<BsonDocument> filteredTells = new List<BsonDocument>();

                if (nao.IsChecked == true)
                {
                    foreach (var tell in tells)
                    {
                        if (tell["robot"] == "nao")
                        {
                            filteredTells.Add(tell);
                        }
                    }
                }
                else if (milo.IsChecked == true)
                {
                    foreach (var tell in tells)
                    {
                        if (tell["robot"] == "milo")
                        {
                            filteredTells.Add(tell);
                        }
                    }
                }
                else
                {
                    foreach (var tell in tells)
                    {
                        filteredTells.Add(tell);
                    }
                }

                foreach (var tell in filteredTells)
                {
                    Console.WriteLine("text: " + tell.ToString());
                }

                if (filteredTells != null)
                {

                    pointsToDrawBefore = new List<Point>();
                    pointsToDrawAfter = new List<Point>();
                    for (int i = 0; i < filteredTells.Count; i++)
                    {
                        double before = filteredTells.ElementAt(i)["happinessBefore"].ToDouble();
                        double after = filteredTells.ElementAt(i)["happinessAfter"].ToDouble();
                        Console.WriteLine("Before: " + before.ToString() + " After: " + after.ToString());
                        double xCoor = i + 1;
                        Point pointBefore = new Point(xCoor, before);
                        Point pointAfter = new Point(xCoor, after);

                        pointsToDrawBefore.Add(pointBefore);
                        pointsToDrawAfter.Add(pointAfter);

                    }

                    foreach (Point p in pointsToDrawAfter)
                    {
                        Console.WriteLine("AFTER: ");
                        Console.WriteLine("X: " + p.X + " Y: " + p.Y);
                    }

                    foreach (Point p in pointsToDrawBefore)
                    {
                        Console.WriteLine("BEFORE: ");
                        Console.WriteLine("X: " + p.X + " Y: " + p.Y);
                    }
                }

                line1.ItemsSource = pointsToDrawAfter;
                line1line.ItemsSource = pointsToDrawAfter;
                line2.ItemsSource = pointsToDrawBefore;
                line2line.ItemsSource = pointsToDrawBefore;

                xAxis.Maximum = pointsToDrawAfter.Count + 1;

                double max = 0;
                foreach (Point p in pointsToDrawAfter)
                {
                    if (p.Y >= max)
                    {
                        max = p.Y;
                    }
                }

                foreach (Point p in pointsToDrawBefore)
                {
                    if (p.Y >= max)
                    {
                        max = p.Y;
                    }
                }
                if (Math.Abs(max - 1.2) < 0.5)
                {
                    max = 1.0;
                }
                yAxis.Maximum = max + 0.1;

                client = new MongoClient(connectionString);
                database = client.GetDatabase("joker-db");
            }
        }

        // points used for drawing - specified in List<Point> pointsToDrawBefore, List<Point> pointsToDrawAfter
        public IEnumerable<Point> Points
        {
            get { return pointsToDrawAfter; }
        }

        // shows all tells for selected joke as text strings in ListBox
        private void plotListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            string selected = plotListBox.SelectedItem.ToString();
            List<BsonDocument> jokes = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();

            var idOfSelected = jokes.Find(x => x["text"] == selected)["_id"];

            Console.WriteLine("Id: " + idOfSelected);

            List<BsonDocument> tells = database.GetCollection<BsonDocument>("tells").Find(x => x["joke"] == idOfSelected && x["happinessBefore"] != 0.5 && x["happinessAfter"] != 0.5).ToList();
            List<string> tellsString = new List<string>();

            for (int i = 0; i < tells.Count; i++)
            {

                Console.WriteLine("Tell: " + tells.ElementAt(i).ToString());

                tellsString.Add(tells.ElementAt(i)["_id"].ToString());
            }

            tellsForMe.ItemsSource = tells;

            client = null;
            database = null;
        }

        // plots all joke's tell's happiness values with respect on selected robot
        private void PlotAllJokes(object sender, RoutedEventArgs e)
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");
            List<BsonDocument> jokes = database.GetCollection<BsonDocument>("jokes").Find(x => true).ToList();

            List<BsonDocument> tells = database.GetCollection<BsonDocument>("tells").Find(x => x["happinessBefore"] != 0.5 && x["happinessAfter"] != 0.5).ToList();
            List<BsonDocument> filteredTells = new List<BsonDocument>();

            if (nao.IsChecked == true)
            {
                foreach (var tell in tells)
                {
                    if (tell["robot"] == "nao")
                    {
                        filteredTells.Add(tell);
                    }
                }
            }
            else if (milo.IsChecked == true)
            {
                foreach (var tell in tells)
                {
                    if (tell["robot"] == "milo")
                    {
                        filteredTells.Add(tell);
                    }
                }
            }
            else
            {
                foreach (var tell in tells)
                {
                    filteredTells.Add(tell);
                }
            }

            foreach (var tell in filteredTells)
            {
                Console.WriteLine("text: " + tell.ToString());
            }

            if (filteredTells != null)
            {

                pointsToDrawBefore = new List<Point>();
                pointsToDrawAfter = new List<Point>();
                for (int i = 0; i < filteredTells.Count; i++)
                {
                    double before = filteredTells.ElementAt(i)["happinessBefore"].ToDouble();
                    double after = filteredTells.ElementAt(i)["happinessAfter"].ToDouble();
                    Console.WriteLine("Before: " + before.ToString() + " After: " + after.ToString());
                    double xCoor = i + 1;
                    Point pointBefore = new Point(xCoor, before);
                    Point pointAfter = new Point(xCoor, after);

                    pointsToDrawBefore.Add(pointBefore);
                    pointsToDrawAfter.Add(pointAfter);

                }

                foreach (Point p in pointsToDrawAfter)
                {
                    Console.WriteLine("AFTER: ");
                    Console.WriteLine("X: " + p.X + " Y: " + p.Y);
                }

                foreach (Point p in pointsToDrawBefore)
                {
                    Console.WriteLine("BEFORE: ");
                    Console.WriteLine("X: " + p.X + " Y: " + p.Y);
                }
            }

            line1.ItemsSource = pointsToDrawAfter;
            line1line.ItemsSource = pointsToDrawAfter;
            line2.ItemsSource = pointsToDrawBefore;
            line2line.ItemsSource = pointsToDrawBefore;

            xAxis.Maximum = pointsToDrawAfter.Count + 1;

            double max = 0;
            foreach (Point p in pointsToDrawAfter)
            {
                if (p.Y >= max)
                {
                    max = p.Y;
                }
            }

            foreach (Point p in pointsToDrawBefore)
            {
                if (p.Y >= max)
                {
                    max = p.Y;
                }
            }
            if (Math.Abs(max - 1.2) < 0.5)
            {
                max = 1.0;
            }
            yAxis.Maximum = max + 0.1;

            client = null;
            database = null;
        }

        // returns funniest joke of all jokes with any tell after 
        private double computeHappinessOfJoke(BsonDocument joke)
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("joker-db");

            double happiness = 0;
            List<BsonDocument> tellsOfJoke = database.GetCollection<BsonDocument>("tells").Find(x => x["joke"] == joke["_id"] && x["happinessBefore"] != 0.5 && x["happinessAfter"] != 0.5).ToList();
            foreach (var tell in tellsOfJoke)
            {
                happiness += tell["happinessAfter"].ToDouble();
                happiness -= tell["happinessBefore"].ToDouble();

            }
            happiness = happiness / tellsOfJoke.Count;

            return happiness;
        }
    }
}
