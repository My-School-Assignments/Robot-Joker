########### Python 2.7 #############
# -*- coding: utf-8 -*-
import httplib, urllib, base64
import pymongo
from datetime import datetime
import random
import time

class Communication:

    headers = {
        # Request headers
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': 'a803ed32771f4d7487b60cbaa8d4f9df',
    }

    params = urllib.urlencode({
    })
    def emoRequest(self,body):
        self.body=body
        try:
            conn = httplib.HTTPSConnection('westus.api.cognitive.microsoft.com')
            conn.request("POST", "/emotion/v1.0/recognize?%s" % self.params, self.body, self.headers)
            response = conn.getresponse()
            data = response.read()
            print(data)
            conn.close()
            return data
        except Exception as e:
            print("[Errno {0}] {1}".format(e.errno, e.strerror))

        ####################################

    def getJokes(self, num = 1):
        uri = "mongodb://joker-db:wsVD6g4NCiun4viFm0xVAzbWZjTyITeQXeapXiLBP5T12DklVzuQl7EqzPigOOyJaHIGtEzFDjB5oEj5OdVdxQ==@joker-db.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"
        client = pymongo.MongoClient(uri)
        db = client["joker-db"]
        recieved = db["jokes"].find()
        jokes = []

        for joke in enumerate(recieved):
            jokes.append(joke)

        random.shuffle(jokes)
        out = []
        i = 0
        while i < num:
            out.append(jokes[i])

        return out

    def sendEmo(self,vtip,pred,po,robot):
        uri = "mongodb://joker-db:wsVD6g4NCiun4viFm0xVAzbWZjTyITeQXeapXiLBP5T12DklVzuQl7EqzPigOOyJaHIGtEzFDjB5oEj5OdVdxQ==@joker-db.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"
        client = pymongo.MongoClient(uri)
        db = client["joker-db"]
        d=dict([("joke",vtip),("happinessBefore",pred,),("happinessAfter",po),("date",time.time()),("robot",robot)])
        db["tells"].insert_one(d)
